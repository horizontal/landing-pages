const gulp = require('gulp');
const gulpIf = require('gulp-if');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const jsImport = require('gulp-js-import');
const sourcemaps = require('gulp-sourcemaps');
const fileinclude = require('gulp-file-include');
const replace = require('gulp-token-replace');
const clean = require('gulp-clean');
const isProd = process.env.NODE_ENV === 'prod';

const configDev = {
  tokens:{
    asset:{
      url:"http://localhost:3000"
    }
  }
}

const configProd = {
  tokens:{
    asset:{
      url:"https://hztl-fed.azureedge.net/holiday-2020"
    }
  }
}

const htmlFile = ['src/*.html'];

function html() {
  return gulp.src(htmlFile)
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    //.pipe(gulpIf(isProd, htmlmin({
    //  collapseWhitespace: true
    //})))
    .pipe(gulpIf(isProd, replace(configProd)))
    .pipe(replace(configDev))
    .pipe(gulp.dest('dist'));
}

function css() {
  return gulp.src('src/sass/style.scss')
    .pipe(gulpIf(!isProd, sourcemaps.init()))
    .pipe(sass({
      includePaths: ['node_modules']
    }).on('error', sass.logError))
    .pipe(gulpIf(isProd, cleanCSS()))
    .pipe(gulpIf(!isProd, sourcemaps.write()))
    .pipe(gulpIf(isProd, replace(configProd)))
    .pipe(replace(configDev))
    .pipe(gulp.dest('dist/css/'));
}

function fonts() {
  return gulp.src('src/fonts/**/*.*')
    .pipe(gulp.dest('dist/fonts/'));
}

function js() {
  return gulp.src('src/js/*.js')
    .pipe(jsImport({
      hideConsole: true
    }))
    .pipe(concat('main.js'))
    .pipe(gulpIf(isProd, uglify()))
    .pipe(gulp.dest('dist/js'));
}

function img() {
  return gulp.src('src/img/**/*.*')
    .pipe(gulpIf(isProd, imagemin()))
    .pipe(gulp.dest('dist/img/'));
}

function serve() {
  browserSync.init({
    open: true,
    server: './dist'
  });
}

function browserSyncReload(done) {
  browserSync.reload();
  done();
}


function watchFiles() {
  gulp.watch('src/**/*.html', gulp.series(html, browserSyncReload));
  gulp.watch('src/**/*.scss', gulp.series(css, browserSyncReload));
  gulp.watch('src/**/*.js', gulp.series(js, browserSyncReload));
  gulp.watch('src/img/**/*.*', gulp.series(img));

  return;
}

function del() {
  return gulp.src('dist/*', {read: false})
    .pipe(clean());
}

exports.css = css;
exports.fonts = fonts;
exports.html = html;
exports.js = js;
exports.del = del;
exports.serve = gulp.parallel(html, css, fonts, js, img, watchFiles, serve);
exports.default = gulp.series(del, html, css, fonts, js, img);
