;(function($){

  var HztlVideo = function($elem){

    var config = {
      selectors: {
        playBtn:'.btn-play-video',
        poster:'.hztl-video-poster'
      },
      classes: {}
    }

    var $container,$poster,$player,$btn;
    var videoPlayer;

    /*
     * Listeners
     */

    var handlePlayBtn = function(e){
      e.preventDefault();

      var vimeoId = $btn.attr('data-video-id')

      if(!$('#vimeo-'+vimeoId).length) {
        createPlayer(vimeoId);
      }

      hideBtn();
      videoPlayer.setLoop(false);
      videoPlayer.setCurrentTime(0);
      videoPlayer.play();
    }

    var handleVideoPlay = function(){
      hidePoster();
    }

    var handleVideoPause = function(){
      showPoster();
      showBtn();
    }

    /*
     * Setup Funcitons
     */

    var getPlayerMarkup = function(vimeoId){
     return $('<div class="embed-responsive-item" id="vimeo-'+vimeoId+'" data-vimeo-id="'+vimeoId+'" data-vimeo-autoplay="false" data-vimeo-loop="false" data-vimeo-color="fa420f" data-vimeo-byline="0" data-vimeo-portrait="0" data-vimeo-title="0"></div>');
    }

    var createPlayer = function(vimeoId){
      $container.prepend( getPlayerMarkup(vimeoId) );
      videoPlayer = new Vimeo.Player('vimeo-'+vimeoId);
      videoPlayer.on('play', handleVideoPlay);
      videoPlayer.on('pause', handleVideoPause);
    }

    /*
     * Utility
     */

    var hidePoster = function(){
     $poster.hide();
    }

    var showPoster = function(){
     $poster.show();
    }

    var hideBtn = function(){
     $btn.hide();
    }

    var showBtn = function(){
     $btn.show();
    }

    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
      $btn = $(config.selectors.playBtn,$container);
      $poster = $(config.selectors.poster,$container);
    }

    var setUp = function(){}

    var attachListeners = function(){
      $btn.on('click',handlePlayBtn);
    }

    return init();

  }

  $(document).ready(function(){
    $('.hztl-video-player').each(function(){
      new HztlVideo( $(this) );
    });
  });

})(jQuery);
