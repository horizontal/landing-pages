;(function($){

  var HztlHolidayGift = function($elem){

    var config = {
      selectors:{},
      classes: {}
    }

    var $gifts;

    /*
     * Listeners
     */



    /*
     * Setup Funcitons
     */


    /*
     * Utility
     */

    var animateGift = function(donate){

      if(donate){
        var tl = gsap.timeline();
        tl.add(createTimeline(donate));
        $gifts.each(function(i){
          if($(this).attr('data-np') !== donate) {
            tl.add(gsap.to($('.hztl-donation',$(this)), {
              duration:.75,
              ease: "bounce.out",
              css:{
                height: randomIntFromInterval(35,60) + '%'
              }
            }), "-=.5");
          }
        });
      } else {
        var tl = gsap.timeline();
        $gifts.each(function(i){
          if(i) {
            tl.add(createTimeline($(this).attr('data-np')), "-=1.5");
          } else {
            tl.add(createTimeline($(this).attr('data-np')));
          }
        });
      }

    }

    var createTimeline = function(donate){

      var $gift = $('.' + donate);
      var tl = gsap.timeline();

      tl.to($gift, {
        delay:1,
        duration:1,
        ease: "elastic.out(1, 0.3)",
        scale:1.25
      });
      tl.to($('.hztl-donation',$gift), {
        duration:.75,
        ease: "bounce.out",
        css:{
          height: randomIntFromInterval(55,90) + '%'
        }
      }, "-=.75");

      return tl;
    }

    var getParameterByName = function(name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
      var results = regex.exec(location.search);
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    var randomIntFromInterval = function(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }


    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $gifts = $elem;
    }

    var setUp = function(){
      var donate = getParameterByName('donate');
      animateGift(donate);
    }

    var attachListeners = function(){}

    return init();

  }


  $(document).ready(function(){
    new HztlHolidayGift($('.hztl-gift'));
  });

})(jQuery);
