;(function($){

  var HztlHolidayStars = function($elem){

    var config = {
      selectors:{},
      classes: {}
    }

    var $container;
    var total = 10;
    var stars = [];

    /*
     * Listeners
     */



    /*
     * Setup Funcitons
     */

    var generateStars = function(){
      var iterator = 0;

      while (iterator <= total){
        createStar();
        iterator++;
      }

      removeStar();

    }

    var createStar = function(){

      var maxHeight = $container.outerHeight();
      var maxWidth = $container.outerWidth();

      var xposition = Math.random();
      var yposition = Math.random();
      var duration = (Math.random() * 4) + 2;

      var position = {
        "x" : maxWidth * xposition,
        "y" : maxHeight * yposition
      };

      var $newstar = $('<span class="star"></span>');

      stars.push($newstar);

      $newstar.appendTo($container).css({
          "top" : position.y,
          "left" : position.x,
          "animationDuration": duration + "s"
      });
    }

    var removeStar = function(){
      var rand = Math.round(Math.random() * 4000);
      setTimeout(function() {
        stars[0].fadeOut(300,function(){
          this.remove();
          stars.shift();
          createStar();
          removeStar();
        });
      }, rand);
    }


    /*
     * Utility
     */


    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
    }

    var setUp = function(){
      generateStars();
    }

    var attachListeners = function(){}

    return init();

  }

  $(document).ready(function(){
    $('.stars').each(function(){
      new HztlHolidayStars($(this));
    });
  });

})(jQuery);
