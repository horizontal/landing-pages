;(function($){

  var HztlHolidaySnow = function($elem){

    var config = {
      selectors:{},
      classes: {}
    }

    var $container;
    var snow = [];

    /*
     * Listeners
     */



    /*
     * Setup Funcitons
     */

    var generateSnowflakes = function(){
      var iterator = 0;
      var total = 5;

      while (iterator <= total){
        createSnowflake();
        iterator++;
      }

      removeSnowflake();

    }


    /*
     * Utility
     */

    var createSnowflake = function(){

      var xposition = Math.random();
      var yposition = (Math.random() * 500);
      var duration = (Math.random() * 9) + 5;

      var position = {
       "x" : $container.width() * xposition,
       "y" : "-" + yposition + "px"
      };

      var $newsnowflake = $('<span class="snowflake"></span>');

      snow.push($newsnowflake);

      $newsnowflake.appendTo($container).css({
         "top" : position.y,
         "left" : position.x,
         "animationDuration": duration + "s"
      });
    }

    var removeSnowflake = function(){
      var rand = Math.round(Math.random() * 12000);
      setTimeout(function() {
       snow[0].fadeOut(300,function(){
         this.remove();
         snow.shift();
         createSnowflake();
         removeSnowflake();
       });
      }, rand);
    }

    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
    }

    var setUp = function(){
      generateSnowflakes();
    }

    var attachListeners = function(){}

    return init();

  }

  $(document).ready(function(){
    $('.snowfall').each(function(){
      new HztlHolidaySnow($(this));
    });
  });

})(jQuery);
