;(function($){

  var HztlAddToCalendar = function($elem){

    var config = {
      selectors:{},
      classes: {}
    }

    var $container,$btn,$dropDown;

    /*
     * Listeners
     */



    /*
     * Setup Funcitons
     */



    /*
     * Utility
     */

    var openDropDown = function(){
      $dropDown.show();
    }

    var closeDropDown = function(){
      $dropDown.hide();
    }

    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
    }

    var setUp = function(){
      createCalendar( $container[0] );
      $btn = $('.btn-hztl-session',$container);
      $dropDown = $('.add-to-calendar-dropdown',$container);
    }

    var attachListeners = function(){
      $btn.on('click',openDropDown);
      $container.on('mouseleave',closeDropDown);
    }

    return init();

  }

  $(document).ready(function(){
    $('.add-to-calendar').each(function(){
      new HztlAddToCalendar($(this));
    });
  });

})(jQuery);
