;(function($){

  var HztlHeartImage = function($elem){

    var config = {
      selectors:{},
      classes: {}
    }

    var svg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 460 460" width="462px" height="462px"><defs><mask id="heartMask-@@id"><rect x="0" y="0" width="460px" height="460px" fill="white"></rect><path d="M229.8,189.9c0.6-0.7,1.3-1.6,2-2.5c1.5-1.8,3-3.7,4.7-5.4c6.8-6.8,14.8-11.3,24.3-12.5c3.7-0.4,7.5-0.3,11.4,0.3c6.3,1,11.9,3.3,16.8,7.2c6.7,5.2,11.2,12,12.8,20.6c1.7,9.4,0.2,18.3-3.8,26.9c-2.8,6-6.3,11.6-10.8,16.4c-5.6,6.1-11.1,12.3-17.1,18c-5,4.8-10.6,9.1-15.9,13.6c-6.8,5.9-14.2,11-21.4,16.3c-1,0.7-2,1.4-3.3,2.3c-5.5-4-11.1-7.9-16.5-12c-5.6-4.3-11.1-8.9-16.5-13.4c-2.6-2.2-5-4.6-7.5-6.9c-2.2-2.1-4.6-4-6.7-6.2c-6.6-6.5-12.9-13.5-17.8-21.6c-4.2-6.8-6.9-14.2-7.7-22.3c-0.7-6.7,0.4-12.9,3-18.9c3.6-8.5,10.3-14,18.7-17.6c5.9-2.6,12.1-3.4,18.3-2.7c9,0.9,17.1,4.3,23.6,10.9C223.5,183.4,226.5,186.5,229.8,189.9" fill="black" /></mask></defs><rect class="mask" x="0" y="0" width="460px" height="460px" mask="url(#heartMask-@@id)" fill="white"></rect><path class="heart" d="M229.8,189.9c0.6-0.7,1.3-1.6,2-2.5c1.5-1.8,3-3.7,4.7-5.4c6.8-6.8,14.8-11.3,24.3-12.5c3.7-0.4,7.5-0.3,11.4,0.3c6.3,1,11.9,3.3,16.8,7.2c6.7,5.2,11.2,12,12.8,20.6c1.7,9.4,0.2,18.3-3.8,26.9c-2.8,6-6.3,11.6-10.8,16.4c-5.6,6.1-11.1,12.3-17.1,18c-5,4.8-10.6,9.1-15.9,13.6c-6.8,5.9-14.2,11-21.4,16.3c-1,0.7-2,1.4-3.3,2.3c-5.5-4-11.1-7.9-16.5-12c-5.6-4.3-11.1-8.9-16.5-13.4c-2.6-2.2-5-4.6-7.5-6.9c-2.2-2.1-4.6-4-6.7-6.2c-6.6-6.5-12.9-13.5-17.8-21.6c-4.2-6.8-6.9-14.2-7.7-22.3c-0.7-6.7,0.4-12.9,3-18.9c3.6-8.5,10.3-14,18.7-17.6c5.9-2.6,12.1-3.4,18.3-2.7c9,0.9,17.1,4.3,23.6,10.9C223.5,183.4,226.5,186.5,229.8,189.9"/><circle class="circle" cx="230" cy="230" r="222" fill="transparent"/></svg>';

    var $container;
    var id;
    var $svg,$mask,$heart,$circle,$maskDef,$maskHeart;

    /*
     * Listeners
     */



    /*
     * Setup Funcitons
     */


    /*
     * Utility
     */

    var animateHeart = function(){
      gsap.to($heart, {
        scrollTrigger:{
          trigger:$container[0],
          start:"top center"
        },
        morphSVG:$circle,
        duration:.75,
        ease: "bounce",
        css:{
          opacity:0
        }
      });

      gsap.to($maskHeart, {
        scrollTrigger:{
          trigger:$container[0],
          start:"top center"
        },
        morphSVG:$circle,
        duration:.75,
        ease: "bounce"
      });

    }

    var fixTheMask = function(){
      MorphSVGPlugin.convertToPath("circle");

      // hacky fix - Safari and IE append base href to mask id making breaky
      var location = window.location.href;
      $mask.attr('mask', 'url(' + location + '#' + $maskDef.attr('id') + ')');
    }

    var createMask = function(){
      var $heartContainer = $('.hztl-heart',$container);
      id = $heartContainer.attr('data-id');

      $heartContainer.append(svg.replace(/@@id/g, id));

      getSvgElements();
    }

    var getSvgElements = function(){
      $svg = $('svg',$container);
      $heart = $('.heart',$svg);
      $circle = $('.circle',$svg);
      $mask = $('.mask',$svg);

      $maskDef = $('mask',$svg);
      $maskHeart = $('path',$maskDef);
    }

    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
    }

    var setUp = function(){
      createMask();
      fixTheMask();
      animateHeart();
    }

    var attachListeners = function(){}

    return init();

  }

  $(document).ready(function(){
    $('.hztl-cares-img').each(function(){
      new HztlHeartImage($(this));
    });
  });

})(jQuery);
