@import '../../node_modules/jquery/dist/jquery.min.js'
@import '../../node_modules/jquery-lazy/jquery.lazy.min.js'
@import '../../node_modules/bootstrap/dist/js/bootstrap.min.js'

@import '../../node_modules/gsap/dist/gsap.min.js'
@import '../../node_modules/gsap/dist/MorphSVGPlugin.min.js'
@import '../../node_modules/gsap/dist/ScrollTrigger.min.js'

@import '../../node_modules/@vimeo/player/dist/player.min.js'

$('.lazy-img').Lazy();
